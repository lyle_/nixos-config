{ inputs, ... }:
{

  jdk = final: prev: {
    # Wrap corretto to use antialiased fonts and GTK Look-and-feel
    jdk = prev.corretto17.overrideAttrs (
      finalAttrs: oldAttrs: {
        name = "corretto17-patched";
        nativeBuildInputs = oldAttrs.nativeBuildInputs or [ ] ++ [ prev.makeWrapper ];
        installPhase =
          oldAttrs.installPhase
          + ''
            # Wrap the java binary
            rm $out/bin/java
            makeWrapper ${prev.corretto17}/bin/java $out/bin/java \
              --add-flags "-Dawt.useSystemAAFontSettings=lcd -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
          '';
      }
    );
  };

  # Contains what you want to overlay
  # You can change versions, add patches, set compilation flags, anything really.
  # https://nixos.wiki/wiki/Overlays
  modifications =
    final: prev:
    let
      mavenConfig = ../config/maven-global-settings.xml;
    in
    {
      # `unstable` is apparently not available in `prev`, so use `final`

      # Ardour 8.8.0 crashes on me, see https://tracker.ardour.org/view.php?id=9814
      ardour = final.unstable.ardour;

      bruno = final.unstable.bruno;

      go = final.unstable.go;

      # Get latest JetBrains releases
      jetbrains.datagrip = final.unstable.jetbrains.datagrip;
      jetbrains.goland = final.unstable.jetbrains.goland;
      jetbrains.idea-ultimate = final.unstable.jetbrains.idea-ultimate;

      maven = final.unstable.maven.overrideAttrs (old: {
        postUnpack = ''
          echo "Copying custom global config (${mavenConfig}) to apache-maven-$version/conf/settings.xml"
          cp ${mavenConfig} apache-maven-$version/conf/settings.xml
        '';
        postInstall =
          (old.postInstall or "")
          + ''
            echo "Overriding default user settings directory to $XDG_CONFIG_HOME/maven/settings.xml"
            wrapProgram $out/bin/mvn      --add-flags "--settings \$XDG_CONFIG_HOME/maven/settings.xml"
            wrapProgram $out/bin/mvnDebug --add-flags "--settings \$XDG_CONFIG_HOME/maven/settings.xml"
          '';
      });

      siril = prev.siril.overrideAttrs (old: rec {
        version = "1.2.3";

        src = prev.fetchFromGitLab {
          owner = "free-astro";
          repo = "siril";
          rev = version;
          hash = "sha256-JUMk2XHMOeocSpeeI+k3s9TsEQCdqz3oigTzuwRHbT4=";
        };

        patches = [ ];

        buildInputs = old.buildInputs ++ [ prev.curl ];

        # Meson fails to find libcurl unless the option is specifically enabled
        configureScript = ''
          ${prev.meson}/bin/meson setup -Denable-libcurl=yes --buildtype release nixbld .
        '';
      });

      # Many Java apps will simply pick up $JAVA_HOME, but others which use scripts configured at build
      # time need to be explicitly overridden.
      soapui = prev.soapui.override { jdk = prev.jdk; };

      vcv-rack = final.unstable.vcv-rack;

      visualvm = prev.visualvm.overrideAttrs (old: {
        # The default build uses `pkgs.java` explicitly instead of the Java version I specify
        installPhase = ''
          find . -type f -name "*.dll" -o -name "*.exe"  -delete;

          substituteInPlace etc/visualvm.conf \
            --replace "#visualvm_jdkhome=" "visualvm_jdkhome=" \
            --replace "/path/to/jdk" "''${JAVA_HOME}" \

          cp -r . $out
        '';
      });
    };

  ## When applied, the unstable nixpkgs set (declared in the flake inputs) will
  ## be accessible through 'pkgs.unstable'
  unstable-packages = final: prev: {
    unstable = import inputs.nixpkgs-unstable {
      system = final.system;
      config.allowUnfree = true;
    };
  };

}
