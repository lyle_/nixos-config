# NixOS Configuration

My configuration for all the machines I run NixOS on.
Lots of inspiration taken from https://github.com/ryan4yin/nix-config/.

See also: [NixOS & Flakes book](https://nixos-and-flakes.thiscute.world/).

[[_TOC_]]

## Layout

| Path | Explanation |
| --------  | ----------- |
| `flake.nix` | the primary entrypoint for NixOS, imports everything else as needed |
| `config` | configuration files for various apps, these will be copied into your Nix config when rebuilding |
| `home` | [home-manager](https://nix-community.github.io/home-manager/index.html) configuration for individual users.<br/>The `hosts` subdirectory contains user configurations specific to particular hosts that might require a different setup (display configuration, font sizes, etc.). |
| `hosts` | configuration for specific hosts |
| `modules` | modules used by multiple hosts for common configurations |
| `overlays` | overlays to modify specific packages, use unstable versions, etc. |
| `secrets` | configuration for secrets encrypted with [agenix](https://github.com/ryantm/agenix) |

## Manual Configuration Steps

There are a few manual steps required to set things up correctly at the moment (hopefully becoming fewer over time):

* Clone this repo onto the new host, saving the previous `/etc/nixos` (if applicable) for reference and/or in case you need to revert
* Copy SSH key for cloning from GitHub into `~/.ssh` and `/root/ssh`, updating the ssh config file if necessary

## Theming

### Wallpaper

The window manager will look for `~/Dropbox/wallpaper/current.[horizontal|vertical]` files based on the host's particular monitor arrangement.
These can be symlinks to actual image files so that active wallpapers can be modified without having to touch the window manager configuration.
