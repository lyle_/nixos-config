{ ... }:

{
  imports = [
    ../../modules/desktop.nix
    ./hardware-configuration.nix
  ];

  networking.enableIPv6 = false;

  hardware = {
    bluetooth = {
      enable = true;
      powerOnBoot = true;
    };
    pulseaudio.enable = false;
  };
  # GTK+ Bluetooth UI widgets
  services.blueman.enable = true;
}
