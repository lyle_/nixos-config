{ ... }:

{
  imports = [
    ../../modules/laptop.nix
    ./hardware-configuration.nix
  ];

  # https://github.com/NixOS/nixpkgs/issues/76671#issuecomment-1581881054
  boot.supportedFilesystems = [ "nfs" ];
}
