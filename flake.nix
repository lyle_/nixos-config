{
  description = "Lyle's NixOS configuration";

  # `inputs` are the dependencies of the flake,
  # Each item in `inputs` will be passed as a parameter to the `outputs` function after being pulled and built.
  inputs = {
    # Official NixOS package source, using nixos's stable branch by default
    nixpkgs.url = "nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";

    # home-manager, used for managing user configuration
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    agenix = {
      url = "github:ryantm/agenix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        darwin.follows = "";
      };
    };

    musnix = {
      url = "github:musnix/musnix";
    };

    mysecrets = {
      # Private repositories seem tricky if you're not using GitHub with a `master` branch.
      # https://github.com/NixOS/nixpkgs/pull/176950
      # Need to make sure the root user has the appropriate SSH key for your repo.
      url = "git+ssh://git@github.com/lhanson/nix-secrets.git?shallow=1";
      flake = false;
    };

    nix-colors.url = "github:misterio77/nix-colors";

    # Logitech device configuration
    solaar = {
      url = "https://flakehub.com/f/Svenum/Solaar-Flake/*.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  # The `outputs` function will return all the build results of the flake.
  # A flake can have many use cases and different types of outputs,
  # parameters in `outputs` are defined in `inputs` and can be referenced by their names.
  # However, `self` is an exception, this special parameter points to the `outputs` itself (self-reference)
  # The `@` syntax here is used to alias the attribute set of the inputs's parameter, making it convenient to use inside the function.
  outputs =
    inputs@{
      self,
      home-manager,
      musnix,
      nix-colors,
      nixpkgs,
      nixpkgs-unstable,
      solaar,
      ...
    }:
    let
      mkSystem =
        hostname:
        nixpkgs.lib.nixosSystem {
          specialArgs = inputs;
          modules = [
            ./secrets
            ./hosts/${hostname}
            {
              networking.hostName = hostname;
            }
            home-manager.nixosModules.home-manager
            {
              home-manager = {
                useGlobalPkgs = true;
                useUserPackages = true;
                backupFileExtension = "backup";
                users.root = import ./home/root.nix;
                users.ldh.imports = [
                  ./home/ldh.nix
                  ./home/hosts/${hostname}.nix
                ];
                extraSpecialArgs = { inherit nix-colors; };
              };
            }
            {
              nixpkgs.config.permittedInsecurePackages = [
                # logseq 0.10.9 requires this.
                # https://github.com/logseq/logseq/issues/11378
                # https://github.com/NixOS/nixpkgs/issues/296939
                "electron-27.3.11"
              ];
            }
            musnix.nixosModules.musnix
            {
              nixpkgs.overlays = [
                self.overlays.jdk
                self.overlays.modifications
                self.overlays.unstable-packages
              ];
            }
            solaar.nixosModules.default
          ];
        };
    in
    {
      overlays = import ./overlays { inherit inputs; };
      nixosConfigurations = {
        laika = mkSystem "laika";
        monty = mkSystem "monty";
      };
    };
}
