{
  config,
  nix-colors,
  osConfig,
  pkgs,
  ...
}:

let
  inherit (nix-colors.lib-contrib { inherit pkgs; }) gtkThemeFromScheme;
  username = "ldh";
  homeDirectory = "/home/ldh";
in
{
  imports = [
    ./modules/all-users.nix
    ./modules/foot.nix
    ./modules/idea.nix
    (import ./modules/waybar ({ inherit username osConfig; }))
    ./modules/node.nix
    ./modules/sway.nix
  ];

  home = {
    inherit username homeDirectory;
    packages = with pkgs; [
      chromium
      diylc
      firefox
      ffmpeg
      fzf
      gimp
      gruvbox-dark-icons-gtk
      kicad
      maestral # Open source Dropbox client
      maestral-gui
      mako
      pavucontrol
      ripgrep
      spotify
      wl-clipboard
    ];
    sessionVariables = {
      CDPATH = ".:/home/ldh:/home/ldh/src";
    };
    shellAliases = {
      ack = "ack --ackrc ${config.xdg.configHome}/ackrc";
      # Go to directory
      gn = "cd /etc/nixos";
      gs = "cd ${homeDirectory}/src";
      gv = "cd /etc/nixos/home/config/nvim";
      # VPN
      splitvpn = "sudo vpn --no-host-names --no-ns-hosts ${config.xdg.configHome}/vpn.config";
      vpn = "sudo vpn ${config.xdg.configHome}/vpn.config";
    };
  };

  gtk = {
    enable = true;

    cursorTheme.name = "Numix-Cursor";
    cursorTheme.package = pkgs.numix-cursor-theme;

    theme.name = "${config.colorScheme.slug}";
    theme.package = gtkThemeFromScheme { scheme = config.colorScheme; };

    # Icon theme set in `packages`

    gtk3 = {
      bookmarks = [
        "file:///mnt/ldh/astro/calibration/QHY268M"
        "file:///home/ldh/Dropbox"
        "file:///home/ldh/src"
        "file:///mnt/data"
        "file:///mnt/ldh"
        "file:///mnt/ldh/astro"
        "file:///tmp"
      ];
    };
  };

  dconf.settings = {
    "org/virt-manager/virt-manager/connections" = {
      autoconnect = [ "qemu:///system" ];
      uris = [ "qemu:///system" ];
    };
  };

  programs = {
    home-manager.enable = true;
    direnv = {
      enable = true;
      enableBashIntegration = true;
      nix-direnv.enable = true;
      config = builtins.fromTOML ''
        [global]
        hide_env_diff = true
      '';
    };
    git = {
      enable = true;
      userName = "Lyle Hanson";
      userEmail = "lyle2.0@gmail.com";
      aliases = {
        a = "add";
        c = "commit";
        d = "diff";
        l = "log";
        s = "status";
      };
      extraConfig = {
        init = {
          defaultBranch = "main";
        };
        url."ssh://git@git.doit.wisc.edu".insteadOf = "https://git.doit.wisc.edu";
      };
      ignores = [
        # Ignore pesky jdt-ls files
        ".classpath"
        ".project"
        ".settings"
      ];
    };
    powerline-go = {
      enable = true;
      modules = [
        "host"
        "ssh"
        "cwd"
        "perms"
        "git"
        "jobs"
        "exit"
      ];
      settings = {
        hostname-only-if-ssh = true;
      };
    };
    ssh = {
      enable = true;
      addKeysToAgent = "yes";
      matchBlocks = {
        "blm.lyler.xyz" = {
          user = "ec2-user";
          identityFile = "~/.ssh/blm-discourse.pem";
        };
        "gitlab.com" = {
          identityFile = "~/.ssh/gitlab";
        };
        "github.com" = {
          identityFile = "~/.ssh/github";
        };
        laika = {
          identityFile = "~/.ssh/laika";
        };
        monty = {
          identityFile = "~/.ssh/monty";
        };
        "prod-admin.enroll.wisc.edu" = {
          user = "ubuntu";
          identityFile = "~/.ssh/enroll-app-prod.key";
        };
        "test-admin.enroll.wisc.edu" = {
          user = "ubuntu";
          identityFile = "~/.ssh/enroll-app-test.key";
        };
        synology = {
          identityFile = "~/.ssh/synology";
          port = 2232;
        };
        astroarch = {
          user = "astronaut";
        };
      };
      extraConfig = ''
        IdentitiesOnly yes
      '';
    };
  };

  xdg.configFile = {
    ackrc.text = ''
      # Ignore compiled artifacts in Maven projects
      --ignore-dir=.idea
      --ignore-dir=target
    '';
    # Enable bash completion of aliased commands
    "bash_init.d/20-alias-completion.sh".source = ./config/complete_alias.bash;
    "bash_init.d/21-alias-completion.sh".text = ''
      # complete the git alias
      complete -F _complete_alias g
    '';
    "bash_init.d/50-environment.sh".text = ''
      # Put go in a less annoying location
      export GOPATH=${config.xdg.dataHome}/go
      # Source GitLab API token
      source ${osConfig.age.secrets.work.path}
    '';
  };

  services.mako = {
    enable = true;
    borderRadius = 5;
    font = "monospace 12";
    layer = "overlay";
    extraConfig = ''
      background-color=#${config.colorScheme.palette.base00}
      text-color=#${config.colorScheme.palette.base05}
      border-color=#${config.colorScheme.palette.base00}
      [urgency=low]
      border-color=#${config.colorScheme.palette.base00}
      [urgency=normal]
      border-color=#${config.colorScheme.palette.base0A}
      [urgency=high]
      border-color=#${config.colorScheme.palette.base08}
    '';
  };

  services.wlsunset = {
    enable = true;
    latitude = "36.1";
    longitude = "-80.2";
  };

  systemd.user.services.maestral = {
    Unit = {
      Description = "Maestral Dropbox client";
      PartOf = [ "graphical-session.target" ];
      After = [ "graphical-session.target" ];
    };
    Service = {
      ExecStart = "${pkgs.maestral-gui}/bin/maestral_qt";
      Restart = "on-failure";
    };
    Install = {
      WantedBy = [ "graphical-session.target" ];
    };
  };

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "23.05";
}
