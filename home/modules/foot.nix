{ config, osConfig, ... }:

let
  footFontSize = hostname:
    if hostname == "laika" then "13"
    else "12";
  fontsize = footFontSize (osConfig.networking.hostName);
in
{
  programs.foot = {
    enable = true;
    settings = {
      main = {
        term = "foot";
        font = "monospace:size=${fontsize}";
        selection-target = "clipboard";
      };

      mouse.hide-when-typing = true;

      colors = {
        # Alpha here applies only to the background, leaving fg text as
        # legible as the compositor opacity allows.
        alpha = "0.5";
        foreground = "${config.colorScheme.palette.base05}"; # Text
        background = "${config.colorScheme.palette.base00}"; # Base

        regular0 = "${config.colorScheme.palette.base03}"; # Surface 1
        regular1 = "${config.colorScheme.palette.base08}"; # red
        regular2 = "${config.colorScheme.palette.base0B}"; # green
        regular4 = "${config.colorScheme.palette.base0A}"; # yellow
        regular3 = "${config.colorScheme.palette.base0D}"; # blue
        regular5 = "f4b8e4"; # pink
        regular6 = "${config.colorScheme.palette.base0C}"; # teal
        regular7 = "b5bfe2"; # subtext 1

        bright0 = "${config.colorScheme.palette.base04}"; # Surface 2
        bright1 = "${config.colorScheme.palette.base08}"; # red
        bright2 = "${config.colorScheme.palette.base0B}"; # green
        bright4 = "${config.colorScheme.palette.base0A}"; # yellow
        bright3 = "${config.colorScheme.palette.base0D}"; # blue
        bright5 = "f4b8e4"; # pink
        bright6 = "${config.colorScheme.palette.base0C}"; # teal
        bright7 = "a5adce"; # subtext 0
      };

      scrollback = {
        lines = 10000;
      };
    };
  };
}
