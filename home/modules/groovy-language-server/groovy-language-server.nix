# A derivation that packages the groovy-language-server
# gradle-env.* are generated with `nix run github:numtide/gradle2nix -- -g 7.3`
# Built against a slightly-tweaked version:
#     https://github.com/lhanson/groovy-language-server/tree/gradle2nix
# to get around a gradle2nix bug that doesn't recognize BOMs:
#     https://github.com/tadfisher/gradle2nix/issues/33
{ pkgs }:

let
  buildGradle = pkgs.callPackage ./gradle-env.nix {};
in
  buildGradle {
    version = "4866a3";
    envSpec = ./gradle-env.json;

    src = builtins.fetchGit {
      url = "https://github.com/GroovyLanguageServer/groovy-language-server";
      rev = "4866a3f2c180f628405b1e4efbde0949a1418c10"; # git commit id
    };

    installPhase = ''
      mkdir -p $out/lib
      # For some reason the compiled JARs are named "source"
      cp build/libs/source-all.jar $out/lib/groovy-language-server-all.jar
    '';
  }
