# XDG Base Directory Specification
# https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
{ config, ...}:

{
  xdg = {
    enable = true;
    userDirs = {
      enable = true;
      desktop = "${config.home.homeDirectory}/.local/share/Desktop";
      documents = "${config.home.homeDirectory}/.local/share/Documents";
      download = /tmp;
      music = /mnt/data/music;
      pictures = "${config.home.homeDirectory}/.local/share/Images";
      videos = "${config.home.homeDirectory}/.local/share/Videos";
      extraConfig = {
        ## Not officially in the specification
        XDG_BIN_HOME = "$HOME/.local/bin";
      };
    };
  };
}
