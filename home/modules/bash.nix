{ config, ... }:
{
  programs.bash = {
    enable = true;
    shellAliases = {
      ssh="TERM=xterm ssh";
      g="git";
      cp="cp -i";
      mv="mv -i";
      rm="rm -i";
      awslocal="alias awslocal='AWS_ACCESS_KEY_ID=fake AWS_SECRET_ACCESS_KEY=fake aws --endpoint-url=http://localhost:4566 --region=us-east-1'";
    };
    historyControl = [ "erasedups" "ignoredups" "ignorespace" ];
    historyFile = "${config.xdg.cacheHome}/bash_history";
    initExtra = ''
      # Source all files needed for bash initialization
      if [ -d ${config.xdg.configHome}/bash_init.d ]; then
        for f in ${config.xdg.configHome}/bash_init.d/*; do source $f; done
      fi

      # Unified bash history http://superuser.com/questions/37576/can-history-files-be-unified-in-bash
      PROMPT_COMMAND="''\${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r"

      # OSC-7 integration for terminals to launch new instances in the current directory
      osc7_cwd() {
        local strlen=''\${#PWD}
        local encoded=""
        local pos c o
        for (( pos=0; pos<strlen; pos++ )); do
            c=''\${PWD:$pos:1}
            case "$c" in
                [-/:_.!\'\(\)~[:alnum:]] ) o="''\${c}" ;;
                * ) printf -v o '%%%02X' "''\${c}" ;;
            esac
            encoded+="''\${o}"
        done
        printf '\e]7;file://%s%s\e\\' "''\${HOSTNAME}" "''\${encoded}"
      }
      PROMPT_COMMAND="''\${PROMPT_COMMAND:+$PROMPT_COMMAND; }osc7_cwd"
    '';
  };
}
