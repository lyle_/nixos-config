# home-manager configuration common to all users
{ nix-colors, ... }:

{
  imports = [
    nix-colors.homeManagerModules.default
    ./bash.nix
    ./neovim.nix
    ./xdg.nix
  ];

  colorScheme = nix-colors.colorSchemes.gruvbox-dark-medium;

}
