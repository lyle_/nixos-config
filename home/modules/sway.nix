{ config, pkgs, ... }:

with config.colorScheme.palette;
let
  # TODO: is there a better way to do this
  extraConfig = pkgs.substituteAll {
    name = "config";
    src = ../config/sway;
    dmenu = "${pkgs.dmenu}";
    findutils = "${pkgs.findutils}";
    runinfocusedscript = runInFocused;
    base00 = base00;
    base01 = base01;
    base02 = base02;
    base03 = base03;
    base04 = base04;
    base05 = base05;
    base06 = base06;
    base07 = base07;
    base08 = base08;
    base09 = base09;
    base0A = base0A;
    base0B = base0B;
    base0C = base0C;
    base0D = base0D;
    base0E = base0E;
    base0F = base0F;
  };
  runInFocused = pkgs.writeShellScriptBin "sway-run-in-focused" ''
    # Run the given command in the current directory of the focused window
    focused=$(swaymsg -t get_tree | jq '.. | select(.focused)? | .pid')
    cd -P /proc/$(pgrep -P "$focused" | head -n 1)/cwd
    exec "$@"
  '';
in {

  home = {
    packages = with pkgs; [
      grim            # screenshot
      runInFocused
      slurp           # select a region for screenshots
      swaybg
      swayidle
      swaylock
    ];
  };

  wayland.windowManager.sway = {
    enable = true;
    config = null;
    extraConfig = builtins.readFile extraConfig;
    wrapperFeatures.gtk = true;
  };

}
