{ osConfig, ... }:

let
  commonRightModules = [
    "pulseaudio"
    "network"
    "cpu"
    "memory"
    "temperature"
    "clock"
    "tray"
  ];
  modulesRight = hostname:
    if hostname == "laika" then
      ["battery"] ++ commonRightModules
    else commonRightModules;
in
{
  imports = [ ./style.css.nix ];

  programs.waybar = {
    enable = true;
    systemd.enable = true;

    settings = [{
      height = 30;
      layer = "bottom";
      position = "top";
      tray = { spacing = 10; };
      modules-left = [ "sway/workspaces" "sway/mode" "sway/scratchpad" ];
      modules-center = [ "sway/window" ];
      modules-right = modulesRight (osConfig.networking.hostName);
      battery = {
        format = "{capacity}% {icon}";
        format-alt = "{time} {icon}";
        format-charging = "{capacity}% ";
        format-icons = [ "" "" "" "" "" ];
        format-plugged = "{capacity}% ";
        states = {
          critical = 15;
          warning = 30;
        };
      };
      clock = {
        format-alt = "{:%Y-%m-%d}";
        tooltip-format = "{:%Y-%m-%d | %H:%M}";
      };
      cpu = {
        format = "{usage}% ";
        tooltip = false;
      };
      memory = { format = "{}% "; };
      network = {
        interval = 1;
        format-alt = "{ifname}: {ipaddr}/{cidr}";
        format-disconnected = "Disconnected ⚠";
        format-ethernet = "{ifname}: {ipaddr}/{cidr}   up: {bandwidthUpBits} down: {bandwidthDownBits}";
        format-linked = "{ifname} (No IP) ";
        format-wifi = "{essid} ({signalStrength}%) ";
      };
      pulseaudio = {
        format = "{volume}% {icon} {format_source}";
        format-bluetooth = "{volume}% {icon} {format_source}";
        format-bluetooth-muted = " {icon} {format_source}";
        format-icons = {
          car = "";
          default = [ "" "" "" ];
          handsfree = "";
          headphones = "";
          headset = "";
          phone = "";
          portable = "";
        };
        format-muted = " {format_source}";
        format-source = "{volume}% ";
        format-source-muted = "";
        on-click = "pavucontrol";
      };
      "sway/mode" = { format = ''<span style="italic">{}</span>''; };
      "sway/scratchpad" = {
        format = ''{icon} {count}'';
        show-empty = false;
        format-icons =  ["" ""];
        tooltip = true;
        tooltip-format = ''{app}: {title}'';
      };
      temperature = {
        critical-threshold = 80;
        format = "{temperatureC}°C {icon}";
        format-icons = [ "" "" "" ];
      };
    }];
  };
}

