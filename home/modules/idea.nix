{ ... }:

{
  programs.git.ignores = [ ".idea" ];

  # Configure ideavim
  xdg.configFile."ideavim/ideavimrc".text = ''
    " Silence the bell
    " http://jason-stillwell.com/blog/2013/04/11/ideavim/
    set visualbell
  '';
}
