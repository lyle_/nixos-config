{ config, pkgs, ... }:

{
  home = {
    packages = with pkgs; [
      nodejs
      nodePackages."@angular/cli"
      nodePackages.npm
    ];
    sessionVariables = {
      NPM_CONFIG_USERCONFIG = "${config.xdg.configHome}/npm";
    };
  };

  xdg.configFile.npm.text = ''
    prefix=${config.xdg.cacheHome}/node_modules
    //git.doit.wisc.edu/api/v4/packages/npm/:_authToken=$GITLAB_TOKEN
  '';
}
