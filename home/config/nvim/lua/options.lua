local o = vim.o
local g = vim.g

g.mapleader = ' '
g.maplocalleader = ' '

g.editorconfig = true
g.loaded_perl_provider = 0
g.loaded_python_provider = 0
g.loaded_ruby_provider = 0

o.clipboard = 'unnamedplus'

o.number = true
o.relativenumber = true

o.signcolumn = 'yes'

o.ignorecase = true
o.smartcase = true

o.tabstop = 4
o.shiftwidth = 4

o.updatetime = 300

o.termguicolors = true

o.mouse = 'a'

o.undofile = true
