return {
  on_attach = function(_, _)
    require('which-key').add({
      { "<leader>a", vim.lsp.buf.code_action,      desc = "Code (a)ction" },
      { "<leader>r", vim.lsp.buf.rename,           desc = "(r)ename" },
      { "<leader>s", require('telescope.builtin').lsp_document_symbols,          desc = "List document (s)ymbols in current buffer" },
      { "<leader>S", require('telescope.builtin').lsp_dynamic_workspace_symbols, desc = "Dynamically lists LSP for all workspace (S)ymbols" },
      { "<leader>K", vim.lsp.buf.hover,            desc = "Display hover information about the symbol under the cursor" },

      -- Group: (c)ode
      { "<leader>c", group = "(c)ode" },

      -- Group: (G)o to
      { "<leader>G", group = "(G)o to" },
      { "<leader>Gd", vim.lsp.buf.definition,      desc = "Go to (d)efinition" },
      { "<leader>GD", vim.lsp.buf.declaration,     desc = "Go to (D)eclaration" },
      { "<leader>GI", vim.lsp.buf.implementation,  desc = "Go to (i)mplementation" },
      { "<leader>GT", vim.lsp.buf.type_definition, desc = "Go to (T)ype definition" },
      { "<leader>Gj", vim.diagnostic.goto_next,    desc = "Go to next diagnostic" },
      { "<leader>Gk", vim.diagnostic.goto_prev,    desc ="Go to previous diagnostic" },
      { "<leader>Gr", '<cmd>Telescope lsp_references<cr>', desc = "Show (r)eferences" },
    })
  end
}
