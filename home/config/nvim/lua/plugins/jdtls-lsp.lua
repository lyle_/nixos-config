----------------------------------------------------------------------
-- nvim-jdtls - Java LSP
-- Config inspired by https://sookocheff.com/post/vim/neovim-java-ide/
----------------------------------------------------------------------

return {
  'mfussenegger/nvim-jdtls',

  ft = 'java',

  config = function(_, _)
    local home = os.getenv('HOME')
    local root_dir = require('jdtls.setup').find_root({'pom.xml'})
    -- eclipse.jdt.ls stores project specific data within a folder. If you are working
    -- with multiple different projects, each project must use a dedicated data directory.
    -- This variable is used to configure eclipse to use the directory name of the
    -- current project found using the root_marker as the folder for project specific data.
    local workspace_folder = home .. "/.local/state/eclipse/" .. vim.fn.fnamemodify(root_dir, ":p")
    local config = {
      flags = {
        debounce_text_changes = 80,
      },

      on_attach = function()
        -- Register common keybindings
        require('modules.lsp-common').on_attach()

        local jdtls = require('jdtls')
        require('which-key').register({
          ['<C-o>'] = { jdtls.organize_imports, "(o)rganize imports" },
          ['<Leader>'] = {
            e = {
              name = "Extract",
              c = { jdtls.extract_constant, "Extract (c)onstant" },
              v = { jdtls.extract_variable, "Extract (v)ariable" },
            },
            t = {
              name = "Test",
              c = { jdtls.test_class, "Test (c)lass" },
              --TODO: configure output: m = { jdtls.test_nearest_method({ config = { console = 'console' }}), "Test (m)ethod" },
              m = { jdtls.test_nearest_method, "Test (m)ethod" },
            }
          }
        })
        require('which-key').register({
          ['<Leader>em'] = { "<ESC><CMD>lua require('jdtls').extract_method(true)<CR>",
            "Extract (m)ethod" }
        }, { mode = "v" })

        -- With `hotcodereplace = 'auto' the debug adapter will try to apply code changes
        -- you make during a debug session immediately.
        jdtls.setup_dap({ hotcodereplace = 'auto' })
        -- Load any launch.json configuration adapters used by Visual Studio Code
        require('dap.ext.vscode').load_launchjs()
        require('jdtls.dap').setup_dap_main_class_configs()
      end,

      -- eclipse.jdt.ls specific settings
      -- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
      settings = {
        java = {
          signatureHelp = { enabled = true },
          contentProvider = { preferred = 'fernflower' },  -- Use fernflower to decompile library code
          -- Specify any completion options
          completion = {
            favoriteStaticMembers = {
              "org.hamcrest.MatcherAssert.assertThat",
              "org.hamcrest.Matchers.*",
              "org.hamcrest.CoreMatchers.*",
              "org.junit.jupiter.api.Assertions.*",
              "java.util.Objects.requireNonNull",
              "java.util.Objects.requireNonNullElse",
              "org.mockito.Mockito.*"
            },
            filteredTypes = {
              "com.sun.*",
              "io.micrometer.shaded.*",
              "java.awt.*",
              "jdk.*", "sun.*",
            },
          },
          -- Specify any options for organizing imports
          sources = {
            organizeImports = {
              starThreshold = 9999;
              staticStarThreshold = 9999;
            },
          },
          -- How code generation should act
          codeGeneration = {
            toString = {
              template = "${object.className}{${member.name()}=${member.value}, ${otherMembers}}"
            },
            hashCodeEquals = {
              useJava7Objects = true,
            },
            useBlocks = true,
          },
        },

      },

      -- See: https://github.com/eclipse/eclipse.jdt.ls#running-from-the-command-line for all options.
      -- Many options are provided by the Nix wrapper for jdt-language-server.
      cmd = {
        'jdt-language-server',
        '-Xmx4g',
        '-configuration', home .. "/.local/share/eclipse/configuration" ,
        '-data', workspace_folder,
      },
    }

    local bundles = {
      vim.fn.glob(home .. "/.local/share/java/debug/com.microsoft.java.debug.plugin.jar", true),
    };

    vim.list_extend(bundles, vim.split(vim.fn.glob(home .. "/.local/share/java/test/*.jar", true), "\n"))
    config['init_options'] = {
      bundles = bundles;
    }

    require('jdtls').start_or_attach(config)
  end
}
