--------------------------
-- Fuzzy finder over lists
--------------------------
return {
  {
    'nvim-telescope/telescope.nvim',

    dependencies = {
      'nvim-lua/plenary.nvim',
      'sharkdp/fd',
      { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
    },

    opts = {
      defaults = {
        layout_config = {
          width = 0.9,
        },
      },
      pickers = {
        colorscheme = { enable_preview = true },
      },
      extensions = {
        file_browser = {
          hijack_netrw = true
        },
        fzf = {
          fuzzy = true,                    -- false will only do exact matching
          override_generic_sorter = true,  -- override the generic sorter
          override_file_sorter = true,     -- override the file sorter
          case_mode = "smart_case",        -- or "ignore_case" or "respect_case"
        }
      }
    },

    init = function()
      require("which-key").add({
        { "<leader>f", group = "Find" },
        { "<leader>fB", "<cmd>Telescope buffers<cr>",         desc = "Find (B)uffers (Telescope)" },
        { "<leader>fb", "<cmd>Telescope file_browser<cr>",    desc = "File (b)rowser (Telescope)" },
        { "<leader>fc", "<cmd>Telescope file_browser path=%:p:h select_buffer=true<cr>", desc = "File browser at (c)urrent path (Telescope)" },
        { "<leader>ff", "<cmd>Telescope find_files<cr>",      desc = "Find (f)iles (Telescope)" },
        { "<leader>fg", "<cmd>Telescope live_grep<cr>",       desc = "Find (g)rep (Telescope)" },
        { "<leader>fH", "<cmd>Telescope search_history<cr>",  desc = "Show search (H)istory (Telescope)" },
        { "<leader>fh", "<cmd>Telescope help_tags<cr>",       desc = "Find (h)elp (Telescope)" },
        { "<leader>fk", "<cmd>Telescope keymaps<cr>",         desc = "Find (k)eymaps (Telescope)" },
        { "<leader>fo", "<cmd>Telescope oldfiles<cr>",        desc = "Show (o)ldfiles list (Telescope)" },
        { "<leader>fs", "<cmd>Telescope grep_string<cr>",     desc = "Find (s)tring (Telescope)" },
        { "<leader>g", group = "Git" },
        { "<leader>gs", "<cmd>Telescope git_status<cr>",      desc = "Git (s)tatus (Telescope)" }
      })
    end,

    config = function(_, opts)
      require('telescope').setup(opts)
      require('telescope').load_extension('file_browser')
      require('telescope').load_extension('fzf')
      require('telescope').load_extension('ui-select')
    end,
  },
  {
    "nvim-telescope/telescope-file-browser.nvim",
    dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },
  },
  'nvim-telescope/telescope-ui-select.nvim',
}
