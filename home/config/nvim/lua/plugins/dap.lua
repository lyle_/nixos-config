-------------------------
-- Debug Adapter Protocol
-------------------------

-- This might be a bit overkill, but since the actions for many key mappings
-- are complex and require other modules to be loaded, it seems like the best
-- way to keep this lazy is to define some local functions for them here.
local toggle_breakpoint = function()
  require('dap').toggle_breakpoint()
end

local open_debug_sidebar = function()
  local widgets = require('dap.ui.widgets')
  widgets.sidebar(widgets.scopes).open()
end

local debug_hover = function()
  require('dap.ui.widgets').hover()
end

local debug_preview = function()
  require('dap.ui.widgets').preview()
end

local show_frames = function()
  local widgets = require('dap.ui.widgets')
  widgets.centered_float(widgets.frames)
end

local show_scopes = function()
  local widgets = require('dap.ui.widgets')
  widgets.centered_float(widgets.scopes)
end

local dap_go = {
  debug_test = function()
    require('dap-go').debug_test()
  end,
  debug_last_test = function()
    require('dap-go').debug_last_test()
  end
}

return {
  -- {
  --   "mfussenegger/nvim-dap",
  --   init = function()
  --     vim.fn.sign_define('DapBreakpoint', {text='🛑', texthl='', linehl='', numhl=''})
  --   end,
  --   keys = {
  --     { "<leader>D", group = "(D)ebug" },
  --     { '<Leader>db',  toggle_breakpoint,   desc = "Toggle (b)reakpoint" },
  --     { '<Leader>df',  show_frames,         desc = "Show (f)rames" },
  --     { '<Leader>dh',  debug_hover,         desc = "Debug (h)over", mode = {'n','v'} },
  --     { '<Leader>dp',  debug_preview,       desc = "Debug (p)review", mode = {'n','v'} },
  --     { '<Leader>ds',  show_scopes,         desc = "Show (s)scopes" },
  --     { '<Leader>dus', open_debug_sidebar,  desc = "Open debugging (s)idebar" },
  --   },
  --   lazy = true
  -- },
  --
  -- {
  --   "leoluz/nvim-dap-go",
  --   ft = "go",
  --   dependencies = "mfussenegger/nvim-dap",
  --   config = true,
  --   keys = {
  --     -- { "<leader>D", group = "(D)ebug" },
  --     { '<Leader>dt', dap_go.debug_test,      desc = 'Debug (t)est' },
  --     { '<Leader>dl', dap_go.debug_last_test, desc = 'Debug (l)ast test' },
  --   }
  -- }
}
