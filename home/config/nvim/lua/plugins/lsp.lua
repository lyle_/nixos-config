---------------------------------
-- Language Server Protocol setup
---------------------------------

-- Load completion capabilities
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)
-- Common LSP attachment and capabilities
local common_config = {
  capabilities = capabilities,
  on_attach = require('modules.lsp-common').on_attach
}
local lspFormattingAugroup = vim.api.nvim_create_augroup("LspFormatting", {})

-- Load Terraform attachment function
local on_attach_terraform = function(_, _)
  -- TODO: terraform fmt on save, see
  -- vim.cmd([[let g:terraform_fmt_on_save=1]])
  -- vim.cmd([[let g:terraform_align=1]])
  require('which-key').add({
    { "<leader>t", group = "Terraform" },
    { "<leader>taa", '<cmd>terminal terraform apply -auto-approve<CR>', desc = "(a)pply, (a)uto-approve" },
    { "<leader>ti", '<cmd>terminal terraform init<CR>',                 desc = "(i)nit" },
    { "<leader>tf", '<cmd>terminal terraform fmt<CR>',                  desc = "(f)mt" },
    { "<leader>tp", '<cmd>terminal terraform plan<CR>',                 desc = "(p)lan" },
    { "<leader>tv", '<cmd>terminal terraform validate<CR>',             desc = "(v)alidate" },
  })
  -- Call the common functionality
  common_config.on_attach(_, _)
end

return {
  'neovim/nvim-lspconfig',

  init = function()
    -- Set filetype for docker-compose so that docker-compose-language-service recognizes it
    vim.api.nvim_create_autocmd(
      { "BufRead" },
      {
        pattern = 'docker-compose.yml',
        callback = function() vim.bo.filetype = 'yaml.docker-compose' end
      }
    )
  end,

  config = function(_, _)
    local lspconfig = require('lspconfig')
    lspconfig.bashls.setup({ common_config })
    lspconfig.docker_compose_language_service.setup({ common_config })
    lspconfig.dockerls.setup({ common_config })
    local gofmt_augrp= vim.api.nvim_create_augroup("goimports", {})
    lspconfig.gopls.setup({
      on_attach = function(client, bufnr)
        -- Format and fix imports on write
        vim.api.nvim_clear_autocmds({ group = gofmt_augrp, buffer = bufnr })
        vim.api.nvim_create_autocmd("BufWritePre", {
          group = gofmt_augrp,
          buffer = bufnr,
          callback = function()
            require('go.format').goimports()
          end,
        })
        -- Common bindings
        common_config.on_attach(client, bufnr)
        -- Go-specific bindings
        require('which-key').add({
          { "<leader>cr", "<cmd>!go run .<cr>", desc = "(r)un" },
        })
      end
    })
    lspconfig.groovyls.setup({
      on_attach = common_config.on_attach,
      capabilities = common_config.capabilities,
      cmd = { "java", "-jar",
        vim.fn.expand("~/.local/share/groovy-language-server-all.jar") },
    })
    lspconfig.jsonls.setup({ common_config })
    lspconfig.lua_ls.setup({
      on_attach = common_config.on_attach,
      capabilities = common_config.capabilities,
      on_init = function(client)
        local path = client.workspace_folders[1].name
        if not vim.loop.fs_stat(path..'/.luarc.json') and not vim.loop.fs_stat(path..'/.luarc.jsonc') then
          client.config.settings = vim.tbl_deep_extend('force', client.config.settings, {
            Lua = {
              runtime = { version = 'LuaJIT' },
              -- Make the server aware of Neovim runtime files
              workspace = {
                checkThirdParty = false,
                library = {
                  vim.env.VIMRUNTIME
                  -- "${3rd}/luv/library"
                  -- "${3rd}/busted/library",
                },
                -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
                -- library = vim.api.nvim_get_runtime_file("", true)
                telemetry = false,
              }
            }
          })

          client.notify("workspace/didChangeConfiguration", { settings = client.config.settings })
        end
        return true
      end
    })
    lspconfig.marksman.setup({ common_config })

    -- Nix files
    lspconfig.nil_ls.setup({
      capabilities = capabilities,
      on_attach = function(client, bufnr)
        vim.api.nvim_clear_autocmds({ group = lspFormattingAugroup, buffer = bufnr })
        vim.api.nvim_create_autocmd("BufWritePre", {
          group = lspFormattingAugroup,
          buffer = bufnr,
          callback = function()
            vim.lsp.buf.format()
          end,
        })
        common_config.on_attach(client, bufnr)
      end,
      settings = {
        -- https://github.com/oxalica/nil/blob/main/docs/configuration.md
        -- https://github.com/oxalica/nil/issues/131#issuecomment-2241281279
        ["nil"] = {
          formatting = {
            command = { "nixfmt" },
          },
          nix = {
            maxMemoryMB = 5120,
            flake = {
              -- calls `nix flake archive` to put a flake and its output to store
              autoArchive = true,
              -- auto eval flake inputs for improved completion
              autoEvalInputs = true,
            },
          },
        },
      },
    })

    lspconfig.terraformls.setup({
      capabilities = capabilities,
      on_attach = on_attach_terraform
    })
    lspconfig.tflint.setup({
      capabilities = capabilities,
      on_attach = on_attach_terraform
    })
    lspconfig.yamlls.setup({
      on_attach = common_config.on_attach,
      capabilities = common_config.capabilities,
      settings = {
        yaml = {
          schemas = {
            ['https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/assets/javascripts/editor/schema/ci.json'] = '/.gitlab-ci.yml'
          }
        }
      }
    })
  end
}
