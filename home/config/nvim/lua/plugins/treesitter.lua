return {
  {
    'nvim-treesitter/nvim-treesitter',
    dependencies = {
      { 'nvim-treesitter/nvim-treesitter-refactor' },
      { 'nvim-treesitter/nvim-treesitter-context' },
    },
    build = ':TSUpdate',

    init = function()
      -- Use TreeSitter folding
      vim.opt.foldmethod = 'expr'
      vim.opt.foldexpr = 'nvim_treesitter#foldexpr()'
      vim.opt.foldtext = 'v:lua.get_foldtext()'
      vim.wo.foldtext = 'v:lua.vim.treesitter.foldtext()'
      vim.opt.foldlevel = 99
    end,

    config = function()
      require('nvim-treesitter.configs').setup {
        ensure_installed = {
          'awk', 'bash', 'clojure', 'comment', 'css', 'csv', 'diff', 'dockerfile',
          'git_config', 'git_rebase', 'gitattributes', 'gitcommit', 'gitignore',
          'go', 'gomod', 'gosum', 'gowork', 'groovy', 'gpg', 'html', 'http', 'java',
          'javascript', 'jq', 'json', 'lua', 'make', 'nix', 'pem', 'sql', 'ssh_config',
          'terraform', 'toml', 'typescript', 'udev', 'vim', 'vimdoc', 'xml', 'yaml'
        },

        auto_install = false,

        highlight = { enable = true },

        indent = { enable = true },

        refactor = {
          highlight_definitions = {
            enable = true,
            -- Set to false if you have an `updatetime` of ~100.
            clear_on_cursor_move = true,
          },
          highlight_current_scope = { enable = false },
          smart_rename = {
            enable = true,
            -- Assign keymaps to false to disable them, e.g. `smart_rename = false`.
            keymaps = {
              smart_rename = "grr",
            },
          },
          navigation = {
            enable = true,
            -- Assign keymaps to false to disable them, e.g. `goto_definition = false`.
            keymaps = {
              goto_definition = "gnd",
              list_definitions = "gnD",
              list_definitions_toc = "gO",
              goto_next_usage = "<A-*>",
              goto_previous_usage = "<A-#>",
            },
          },
        },
      }
    end
  },
}
