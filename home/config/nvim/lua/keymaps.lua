-- General keymaps
-- Relies on bufferline.nvim, doesn't work in netrw
vim.keymap.set("n", "<C-h>", ":BufferLineCyclePrev<CR>",
  { silent = true, desc = "Cycle to previous buffer" })
vim.keymap.set("n", "<C-l>", ":BufferLineCycleNext<CR>",
  { silent = true, desc = "Cycle to next buffer" })
