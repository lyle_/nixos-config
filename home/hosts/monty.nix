{ pkgs, ... }:

{
  wayland.windowManager.sway = {
    # checkConfig will fail because it sees ~ as /homeless-shelter
    checkConfig = false;

    extraConfig = ''

      ### Settings specific to monty
      output "Dell Inc. DELL S2721Q 8XR5N43" {
        transform 90
        mode 2560x1440
        pos 0 0
        bg ~/Dropbox/wallpaper/current.vertical fill
      }

      output "LG Electronics 34GL750 0x00073D2C" {
        mode 2560x1080
        pos 1440 855
        bg ~/Dropbox/wallpaper/current.horizontal fill
      }

    '';
  };

  xdg.dataFile = {
    # Link the starnet directory to a stable path I can configure in Siril
    starnet.source = (pkgs.callPackage ../../modules/starnet.nix {}) + "/bin";
  };

}
