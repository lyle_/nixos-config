{ ... }:

{
  wayland.windowManager.sway = {
    extraConfig = ''

      ### Settings specific to laika
      # Synaptics Touchpad
      input "1739:0:Synaptics_tm2964-001" {
        dwt              enabled      # disable while typing
        tap              enabled
        middle_emulation enabled
      }

      output "eDP-1" {
        bg ~/Dropbox/wallpaper/current.horizontal fill
      }

    '';
  };
}

