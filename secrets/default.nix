# import & decrypt secrets in `mysecrets` in this module
{ agenix, mysecrets, ... }:

{
  imports = [
     agenix.nixosModules.default
  ];

  age.secrets.maven= {
    path = "/home/ldh/.config/maven/settings.xml";
    file = "${mysecrets}/maven.age";
    mode = "0400";
    owner = "ldh";
    group = "users";
  };

  age.secrets.vpn = {
    path = "/home/ldh/.config/vpn.config";
    file = "${mysecrets}/vpn.age";
    mode = "0400";
    owner = "ldh";
    group = "users";
  };

  age.secrets.wifi = {
    file = "${mysecrets}/wifi.age";
    mode = "0400";
    owner = "root";
    group = "root";
  };

  age.secrets.work = {
    path = "/home/ldh/.config/work.config";
    file = "${mysecrets}/work.age";
    mode = "0400";
    owner = "ldh";
    group = "users";
  };

  age.secrets.pia-credentials = {
    file = "${mysecrets}/pia/credentials.age";
  };
  age.secrets.pia-ca-montreal = {
    file = "${mysecrets}/pia/ca-montreal.age";
  };
  age.secrets.pia-guatemala = {
    file = "${mysecrets}/pia/guatemala.age";
  };
  age.secrets.pia-us-dc = {
    file = "${mysecrets}/pia/us-dc.age";
  };
  age.secrets.pia-us-virginia = {
    file = "${mysecrets}/pia/us-virginia.age";
  };
}
