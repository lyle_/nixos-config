# Common configuration for desktop environments
{ pkgs, ... }:

{
  imports = [
    # Desktops get all normal desktop functionality
    ./system.nix
  ];

  # Set your time zone.
  time.timeZone = "America/New_York";

  # System packages
  environment.systemPackages = with pkgs; [
    kdenlive
    (pkgs.callPackage ./tagspaces.nix {})
    shotwell                            # Image organizer/camera access tool
    siril                               # Astrophotography image processing
    (pkgs.callPackage ./starnet.nix {}) # Used by siril
  ];

}
