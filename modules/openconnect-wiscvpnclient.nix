# A derivation that installs an OpenConnect VPN client for WiscVPN
{ stdenvNoCC }:

stdenvNoCC.mkDerivation rec {
  pname = "openconnect-wiscvpnclient";
  version = "47b663";

  src = builtins.fetchGit {
    url = "https://github.com/lhanson/openconnect-wiscvpn-client.git";
    rev = "484e5500c9fd4138d16dc348776b34353e495cc6"; # git commit id
  };

  installPhase = ''
    runHook preInstall
    install -D --mode 555 vpn scripts/hipreport.sh scripts/vpnc-script --target-directory $out/bin
    runHook postInstall
  '';

  meta = {
    homepage = "https://github.com/lhanson/openconnect-wiscvpn-client";
    description = "OpenConnect WiscVPN client";
    version = version;
    longDescription = ''
      A wrapper for connecting to WiscVPN using OpenConnect. Supports MFA (Duo Push) and split tunneling.
    '';
  };
}
