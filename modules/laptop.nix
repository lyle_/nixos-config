# Common configuration for desktop environments
{ ... }:

{
  imports = [
    # Laptops get all normal desktop functionality
    ./system.nix
  ];


  services.automatic-timezoned.enable = true;

  programs.light.enable = true;
  programs.nm-applet.enable = true;

  # Wifi
  networking = {
    networkmanager.enable = true;
  };

  users.users.ldh = {
    extraGroups = [ "networkmanager" ];
  };
}
