# Astrophotography-related software
{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    indi-full
    kstars
    remmina     # VNC client for connecting to astroarch
    stellarium  # 3D map of the sky
  ];
}
