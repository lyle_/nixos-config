# Java version and configuration
{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    maven
    soapui
    visualvm
  ];
  programs.java = {
    enable = true;
  };

  environment = {
    sessionVariables = {
      # Fix Java apps on Wayland, otherwise some display a blank screen when opened.
      _JAVA_AWT_WM_NONREPARENTING = "1";
    };
  };

  fileSystems = {
    # Misbehaving applications/plugins sometimes still try to write to ~/.m2
    # https://youtrack.jetbrains.com/issue/IDEA-182782/Maven-User-Settings-file-and-Local-repository-paths-are-ignored.
    "/home/ldh/.m2/repository" = {
      device = "/home/ldh/.cache/maven";
      fsType = "none";
      options = [ "bind" ];
    };

    # Running the Maven projects in Docker requires a Maven repository with any local changes being tested.
    # Mount the local repository into the project to avoid a lot of hassle.
    "/home/ldh/src/caos/soap/.m2/repository" = {
      device = "/home/ldh/.cache/maven";
      fsType = "none";
      options = [ "bind" ];
    };
    "/home/ldh/src/caos/etl/.m2/repository" = {
      device = "/home/ldh/.cache/maven";
      fsType = "none";
      options = [ "bind" ];
    };
  };
}
