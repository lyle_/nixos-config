{ pkgs, ... }:

# TODO: why is this pkgs.runCommand vs. an overlay?
# let
#   soapui = pkgs.runCommand "soapui" {
#     buildInputs = [ pkgs.makeWrapper ];
#   } ''
#     mkdir $out
#     # Link every top-level folder from pkgs.hello to our new target
#     ln -s ${pkgs.soapui}/* $out
#     # Except the bin folder
#     rm $out/bin
#     mkdir $out/bin
#     # We create the bin folder ourselves and link every binary in it
#     ln -s ${pkgs.soapui}/bin/* $out/bin
#     # Except the binary to be wrapped
#     rm $out/bin/soapui
#     # Because we create this ourself, by creating a wrapper
#     # TODO: is this needed, because we set this in java.nix?
#     makeWrapper ${pkgs.soapui}/bin/soapui $out/bin/soapui \
#       --set _JAVA_AWT_WM_NONREPARENTING 1
#   '';
# in
{
#   # System packages
#   environment.systemPackages = [
#     soapui
#   ];
}
