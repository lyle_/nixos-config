# Configuration common to all systems
{
  config,
  lib,
  agenix,
  pkgs,
  ...
}:

{
  imports = [
    ./astrophotography.nix
    ./daw.nix
    ./java.nix
    ./nfs.nix
    ./soapui.nix
  ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.ldh = {
    isNormalUser = true;
    extraGroups = [
      "dialout"
      "docker"
      "input"
      "media"
      "libvirtd"
      "uinput"
      "video"
      "wheel"
    ]; # Enable ‘sudo’ for the user.
  };
  # Define the 'media' group used by Synology shares
  users.groups.media.gid = 65536;

  nix = {
    package = pkgs.nixVersions.stable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    # https://nixos.wiki/wiki/Storage_optimization
    # do garbage collection weekly to keep disk usage low
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };
  };

  nixpkgs.config.allowUnfree = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot = {
      enable = true;
      # Limit the number of stored generations
      configurationLimit = lib.mkDefault 10;
      memtest86.enable = true;
    };
    efi.canTouchEfiVariables = true;
  };

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    ack
    agenix.packages.${system}.agenix
    curl
    file
    htop
    logseq
    nfs-utils
    openconnect
    (pkgs.callPackage ./openconnect-wiscvpnclient.nix { })
    teams-for-linux
    tree
    unzip
    usbutils
    virt-manager
    vlc
    vpn-slice
    wget
    xdg-utils
    xfce.thunar
    xfce.thunar-volman
    xfce.tumbler
    zoom-us

    # Development
    awscli2
    bruno
    cmake
    foot
    git
    gcc
    gnumake
    go
    jetbrains.datagrip
    jetbrains.goland
    jetbrains.idea-ultimate
    jq
    lua-language-server
    ssm-session-manager-plugin
    terraform
  ];

  # Specified for openconnect-wiscvpnclient.nix's vpn-split since this is what it tries to write to /etc/hosts anyway
  networking.extraHosts = ''
    144.92.254.254 dns0.tun0		# for openconnect-wiscvpnclient.nix (vpn-slice-tun0)
    128.104.254.254 dns1.tun0		# for openconnect-wiscvpnclient.nix (vpn-slice-tun0)
  '';

  environment.etc."inputrc" = {
    text = pkgs.lib.mkDefault (
      pkgs.lib.mkAfter ''
        set editing-mode vi
        # Indicate current editing mode
        set show-mode-in-prompt On
        ## Mode indicators, see https://stackoverflow.com/a/42107711/80144
        # Cursor is a blinking bar in insert mode
        set vi-ins-mode-string \1\e[5 q\2
        # Cursor is a steady block in command mode
        set vi-cmd-mode-string \1\e[2 q\2
      ''
    );
  };

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = false;
  };
  services.solaar.enable = true;
  programs = {
    dconf.enable = true; # virt-manager requires dconf to remember settings
    neovim = {
      enable = true;
      defaultEditor = true;
      viAlias = true;
      vimAlias = true;
    };
    ssh.startAgent = true;
  };

  security.sudo.extraConfig = ''
    Defaults timestamp_timeout=60
  '';

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Key remapping
  services.interception-tools = {
    enable = true;
    plugins = [ pkgs.interception-tools-plugins.dual-function-keys ];
    udevmonConfig = ''
      - JOB: "${pkgs.interception-tools}/bin/intercept -g $DEVNODE | ${pkgs.interception-tools-plugins.dual-function-keys}/bin/dual-function-keys -c /etc/nixos/config/dual-function-keys.yaml | ${pkgs.interception-tools}/bin/uinput -d $DEVNODE"
        DEVICE:
          EVENTS:
            EV_KEY: [KEY_LEFTCTRL]
    '';
  };

  services.udev.extraHwdb = ''
    # Keyboard mappings
    # Matching format:
    #     evdev:input:b<bus_id>v<vendor_id>p<product_id>e<version_id>-<modalias>
    # Assignment format:
    #     KEYBOARD_KEY_[scancode]=keycode
    # See:
    #     http://www.comptechdoc.org/os/linux/howlinuxworks/linux_hlkeycodes.html
    #     https://wiki.archlinux.org/index.php/Map_scancodes_to_keycodes#Example_for_custom_hwdb

    # build-in keyboard: match all AT keyboards for now
    evdev:atkbd:dmi:*
     KEYBOARD_KEY_2b=backspace        # bind pipe to backspace
     KEYBOARD_KEY_0e=backslash        # bind backspace to pipe

    # KBT Pure, identified by Holtek Semiconductor controller
    evdev:input:b0003v04D9p0134*
     KEYBOARD_KEY_700e2=leftctrl      # Alt_L to Ctrl_L so that alt is next to spacebar
     KEYBOARD_KEY_700e3=leftalt       # Ctrl_L to Alt_L
     KEYBOARD_KEY_70031=backspace     # bind pipe to backspace
     KEYBOARD_KEY_7002a=backslash     # bind backspace to pipe

  '';

  virtualisation = {
    docker = {
      enable = true;
      autoPrune = {
        enable = true;
        dates = "monthly";
      };
    };
    libvirtd.enable = true;
    spiceUSBRedirection.enable = true;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

  # Enable sound.
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  # Wayland/Sway
  security.polkit.enable = true;
  hardware.graphics.enable = true;
  security.pam.services.swaylock.text = ''
    auth include login
  '';

  # xdg-desktop-portal works by exposing a series of D-Bus interfaces
  # known as portals under a well-known name
  # (org.freedesktop.portal.Desktop) and object path
  # (/org/freedesktop/portal/desktop).
  # The portal interfaces include APIs for file access, opening URIs,
  # printing and others.
  services.dbus.enable = true;
  xdg.portal = {
    enable = true;
    # https://github.com/flatpak/xdg-desktop-portal/blob/1.18.1/doc/portals.conf.rst.in
    config.common.default = "*";
    wlr.enable = true;
    # gtk portal needed to make gtk apps happy
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };

  # Enable CUPS to print documents.
  users.groups = {
    uinput = { };
  };
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.brlaser ];
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    openFirewall = true;
  };

  fonts = {
    fontconfig.enable = true;
    packages = with pkgs; [
      inter
      nerdfonts
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji
    ];
  };

  # Greeter
  services.greetd = {
    enable = true;
    settings = rec {
      initial_session = {
        command = "${pkgs.sway}/bin/sway";
        user = "ldh";
      };
      default_session = initial_session;
    };
  };

  services.gnome.gnome-keyring.enable = true;

  programs = {
    # Gnome Keyring application
    seahorse.enable = true;

    steam.enable = true;
  };

  services.openvpn.servers = {
    pia-ca-montreal = {
      autoStart = false;
      config = ''
        config ${config.age.secrets.pia-ca-montreal.path}
        auth-user-pass ${config.age.secrets.pia-credentials.path}
      '';
    };
    pia-guatemala = {
      autoStart = false;
      config = ''
        config ${config.age.secrets.pia-guatemala.path}
        auth-user-pass ${config.age.secrets.pia-credentials.path}
      '';
    };
    pia-us-dc = {
      autoStart = false;
      config = ''
        config ${config.age.secrets.pia-us-dc.path}
        auth-user-pass ${config.age.secrets.pia-credentials.path}
      '';
    };
    pia-us-virginia = {
      autoStart = false;
      config = ''
        config ${config.age.secrets.pia-us-virginia.path}
        auth-user-pass ${config.age.secrets.pia-credentials.path}
      '';
    };
  };

  environment.etc."greetd/environments".text = ''
    sway
  '';

}
