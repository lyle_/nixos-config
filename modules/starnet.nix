{ stdenv, fetchzip, autoPatchelfHook }:

stdenv.mkDerivation rec {
  pname = "starnet";
  version = "2.0.2";

  src = fetchzip {
    url = "https://starnetastro.com/wp-content/uploads/2022/03/StarNetv2CLI_linux.zip";
    sha256 = "sha256-qDaJrsBUnR4kuIqsfJ9ijAtH3ee1aprfVTmZuLu+EZ4=";
  };

  nativeBuildInputs = [
    autoPatchelfHook
  ];

  buildInputs = [
    stdenv.cc.cc.lib
  ];

  prebuild = ''
    ls
    addAutoPatchelfSearchPath source
  '';

  installPhase = ''
    runHook preInstall
    install -m755 -D starnet++ $out/bin/starnet++
    cp *.so.* starnet2_weights.pb $out/bin
    runHook postInstall
  '';

  meta = {
    homepage = "https://www.starnetastro.com";
    description = "A neural network that can remove stars from images in one simple step leaving only background.";
    version = version;
    longDescription = ''
      A convolutional residual net with encoder-decoder architecture and with L1, Adversarial and Perceptual losses.
    '';
  };
}
