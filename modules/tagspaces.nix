{ pkgs, appimageTools }:

appimageTools.wrapType2 {
  name = "tagspaces";
  version = "5.9.2";

  src = pkgs.fetchurl {
    url = "https://github.com/tagspaces/tagspaces/releases/download/v5.9.2/tagspaces-linux-x86_64-5.9.2.AppImage";
    hash = "sha256-HsqGQUAe+Rtnarukck6ZwtCXFSD3JOFmBZLj/RLgOq8=";
  };
}
