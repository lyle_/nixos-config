# NFS mounts
{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    nfs-utils
  ];

  services.rpcbind.enable = true; # needed for NFS
  systemd.mounts = let commonMountOptions = {
    type = "nfs";
    mountConfig = {
      Options = "noatime";
    };
  };

  in [
    (commonMountOptions // {
      what = "synology:/volume1/data";
      where = "/mnt/data";
    })

    (commonMountOptions // {
      what = "synology:/volume1/Lyle";
      where = "/mnt/ldh";
    })
  ];

  systemd.automounts = let commonAutoMountOptions = {
    wantedBy = [ "multi-user.target" ];
    automountConfig = {
      TimeoutIdleSec = "600";
    };
  };

  in [
    (commonAutoMountOptions // { where = "/mnt/data"; })
    (commonAutoMountOptions // { where = "/mnt/ldh"; })
  ];
}
