# Audio and DAW-related configuration
{ pkgs, ... }:

{
  users.users.ldh = {
    extraGroups = [ "audio" ];
  };
  environment.systemPackages = with pkgs; [
    ardour
    calf
    carla
    guitarix
    helm         # polyphonic synthesizer
    lsp-plugins
    tuxguitar
    vcv-rack
    vital
  ];
  musnix.enable = true;

  security.pam.loginLimits = [
    # Ardour complains when it can lock less than full sytem memory
    { domain = "@audio"; item = "memlock"; type = "-"; value = "unlimited"; }
  ];
}
